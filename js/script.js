(function ($) {
    function filterClasses () {

        $('input').click(function () {
            $('.active').removeClass('active');
            $(this).parent().parent().addClass('active');
            $('input:not(:checked)').parentsUntil('.sf-field-category', 'li').removeClass("active-trail");
            $('input:checked').parentsUntil('.sf-field-category', 'li').addClass("active-trail");
        });

    }

    $(document).ready(function() {
       filterClasses();
    });
})(jQuery);